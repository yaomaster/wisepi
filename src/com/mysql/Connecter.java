package com.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class Connecter
{
    public static void main(String[] args)
    {
        Connection conn = null;
        try
        {
            //JDBC connect to database example

            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("連接成功MySQLToJava");

            String datasource = "jdbc:mysql://localhost:9090/phpmyadmin/vote?user=root&password=f53060509";

            conn = DriverManager.getConnection(datasource);
            System.out.println("連接成功MySQL");
            Statement st = conn.createStatement();

            st.execute("SELECT * FROM member");
            ResultSet rs = st.getResultSet();
            while(rs.next())
            {
                System.out.println(rs.getString("account")+" "+rs.getString("pwd"));
            }
        }catch(Exception e)
        {
            System.out.println("Error Connect to DataBase");
        }
    }
}