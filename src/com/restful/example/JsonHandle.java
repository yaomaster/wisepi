package com.restful.example;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mclab on 2017/7/26.
 */
public class JsonHandle {
    public String json(InputStream inputString)
    {
        StringBuilder builder = new StringBuilder();
        try
        {
            BufferedReader input = new BufferedReader(new InputStreamReader(inputString));
            String line = null;
            while ((line = input.readLine()) != null) {
                builder.append(line);
            }
        }
        catch(Exception e)
        {

            System.out.println("Error Pasing!!");
        }
        return builder.toString();
    }
}
