package com.restful.example;
/**
 * Created by mclab on 2017/7/20.
 */
 
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
 
import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 
@Path("/")
public class RESTService
{

	JsonHandle jsonhandler;

	@POST
	@Path("/Wise/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response LoginHandles(InputStream incomingData )
	{
		//String builder = "";
		StringBuilder builder = new StringBuilder();
		if(incomingData==null)
		{
			System.out.println("No Data Receive");
		}
		else
		{
			try
			{
				BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
				String line = null;
				while ((line = in.readLine()) != null)
				{
					builder.append(line);
				}
			}
			catch(Exception e)
			{
				System.out.println("Error Pasing");
			}

			//jsonhandler.json(incomingData);
			System.out.println("====================================");
			System.out.println("Data Received: " + builder.toString());
		}

		// return HTTP response 200 in case of success
		return Response.status(200).entity(builder.toString()).build();
	}

	@POST
	@Path("/Wise/pi")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response PiVerify(InputStream incomingData)
	{
		StringBuilder builder = new StringBuilder();
		if(incomingData==null)
		{
			System.out.println("No Data");
		}
		else
		{
			try
			{
				BufferedReader input = new BufferedReader(new InputStreamReader(incomingData));
				String line = null;
				while ((line = input.readLine()) != null)
				{
					builder.append(line);
				}
			}
			catch(Exception e)
			{

				System.out.println("Error");
			}
			//jsonhandler.json(incomingData);
			System.out.println("====================================");
			System.out.println("Data Received: " + builder.toString());

		}
		return Response.status(200).entity(builder.toString()).build();
	}

	@GET
	@Path("/Wise/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService()
	{
		String result = "userid";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/Wise/test")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTServices()
	{
		String result = "test.......";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
	
	
	@GET
	@Path("/Wise/user")
	@Produces(MediaType.TEXT_PLAIN)
	public Response userVerifty()
	{
		String result = "";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/Wise/Unity")
	@Produces(MediaType.TEXT_PLAIN)
	public Response UnityVerify()
	{
		String result = "";
		return Response.status(200).entity(result).build();
	}
	
 
}