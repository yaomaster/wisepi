package com.restful.example;
 
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
 
//import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONObject;


public class RestfulClient
{
    // User post info about account and pwd
	public void UserPost()
	{
		String string = "";
		try
        {

			InputStream InputStream = new FileInputStream("/Users/mclab/Desktop/UserInfo.json");
			InputStreamReader Reader = new InputStreamReader(InputStream);
			BufferedReader br = new BufferedReader(Reader);
			String line;
			while ((line = br.readLine()) != null)
            {
				string += line + "\n";
			}
			JSONObject jsonObject = new JSONObject(string);
			//System.out.println(jsonObject);
			try
            {
				URL url = new URL("http://localhost:8080/Wise/login");
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(5000);
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
				out.write(jsonObject.toString());
				out.close();
 
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
 
				while (in.readLine() != null) {
				}
				System.out.println("\nREST Services Invoked Successfully..");
				in.close();
			} catch (Exception e) {
				System.out.println("\nError while calling REST Service");
				System.out.println(e);
			}
 
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void PiPost()
	{
		String string = "";
		try {

			InputStream InputStream = new FileInputStream("/Users/mclab/Desktop/pi_info.json");
			InputStreamReader Reader = new InputStreamReader(InputStream);
			BufferedReader br = new BufferedReader(Reader);
			String line;
			while ((line = br.readLine()) != null) {
				string += line + "\n";
			}
 
			//JSONObject jsonObject = new JSONObject(string);
			//System.out.println(jsonObject);
 

			try {
				URL url = new URL("http://localhost:8080/Wise/pi");
				//Url url = new URl("http://140.119.163.193:8080");
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(5000);
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
				//out.write(jsonObject.toString());
				out.close();
 
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
 
				while (in.readLine() != null) {
				}
				System.out.println("\nREST Service Invoked Successfully..");
				in.close();
			} catch (Exception e) {
				System.out.println("\nError while calling  REST Service");
				System.out.println(e);
			}
 
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void userGet()
	{
		Client c = Client.create();
        WebResource resource = c.resource("http://localhost:8080/CrunchifyTutorials/api/user");
        String response = resource.get(String.class);
        System.out.println(response);
	}
	public void PiGet()
	{
		Client c = Client.create();
        WebResource resource = c.resource("http://localhost:8080/Restful/api/Pi");
        String response = resource.get(String.class);
        System.out.println(response);
	}
	
	public static void main(String[] args)
    {
//		Client c = Client.create();
//        WebResource resource = c.resource("http://localhost:8080/Wise/test");
//        String response = resource.get(String.class);
//        System.out.println(response);
        String string = "";
        try
        {

            InputStream InputStream = new FileInputStream("/Users/mclab/Desktop/pi_info.json");
            InputStreamReader Reader = new InputStreamReader(InputStream);
            BufferedReader br = new BufferedReader(Reader);
            String line;
            while ((line = br.readLine()) != null)
            {
                string += line + "\n";
            }
            JSONObject jsonObject = new JSONObject(string);
            //System.out.println(jsonObject);
            try
            {
                URL url = new URL("http://localhost:8080/Wise/login");
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write(jsonObject.toString());
                out.close();

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                while (in.readLine() != null) {
                }
                System.out.println("\nREST Service Invoked Successfully..");
                in.close();
            } catch (Exception e) {
                System.out.println("\nError while calling  REST Service");
                System.out.println(e);
            }

            br.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	
	
	
}